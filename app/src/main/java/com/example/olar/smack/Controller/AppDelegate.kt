package com.example.olar.smack.Controller

import android.app.Application
import com.example.olar.smack.Utils.SharedPrefs

class AppDelegate: Application() {

    //A singleton in a specific class
    companion object {
        lateinit var prefs: SharedPrefs
    }

    override fun onCreate() {
        prefs = SharedPrefs(applicationContext)
        super.onCreate()
    }
}